const config = require('config')
const jwt = require('jsonwebtoken');

var auth = function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token']

    if (!token) {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        })
    }

    jwt.verify(token, config.get('token.privatekey'), function(err, decoded) {
        if (err) {
            return res.send({
                success: false,
                message: 'Failed to authenticate token.'
            })
        }
        req.body.decoded = decoded;
        next();

    })
}

module.exports = auth