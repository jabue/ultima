const config = require('config')
const acl = require('express-acl')

let configObject = {
    rules: config.get('acl'),
    decodedObjectName: 'decoded',
    searchPath: 'user.role'
}

let responseObject = {
    status: 'Access Denied',
    message: 'You are not authorized to access this resource'
}

acl.config(configObject, responseObject)

module.exports = acl.authorize