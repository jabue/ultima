require('app-module-path').addPath(__dirname + '/../')
const express = require('express')
const config = require('config')
const bodyParser = require('body-parser')
const multer = require('multer');
const moduleLoader = require('boot/ModuleLoader')

const upload = multer();
const app = express()
//parse x form data
app.use(bodyParser.urlencoded({ extended: false }))
//parse json data
app.use(bodyParser.json())
//parse form data
app.use(upload.array());

//load all modules from app folder
const modules = moduleLoader.getAllModules()

moduleLoader.getAllRoutes().forEach(function(route) {
    app.use('/rest', route)
})

app.listen(3000, function() {
    console.log('Server started on port 3000')
})