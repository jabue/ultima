const loader = require('auto-loader')
const modules = loader.load(process.cwd() + '/app')

var exports = module.exports = {}
var routes
var tests

exports.getAllModules = function() {
    return modules
}

exports.getModule = function(moduleName) {
    return modules[moduleName];
}

exports.getAllRoutes = function() {
    if (!routes) {
    	routes = Object.values(modules).map(e => e.Route.Api)
    }
    return routes
}

exports.getAllTests = function() {
    if (!tests) {
    	tests = Object.values(modules).map(e => e.Test)
    }
    return tests
}