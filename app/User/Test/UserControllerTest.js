require('app-module-path').addPath(__dirname + '/../../../')
process.env.NODE_CONFIG_DIR = '../../../config'
process.env.NODE_ENV = 'dev'
const User = require('app/User/Model/User')
var assert = require('assert')
describe('UserController', function() {
    describe('getList', function() {
        it('should get all users without error', function(done) {
            User.fetchAll().then(function(data) {
                if (data) done()
                else done(new Error('Get all users failed.'))
            })
        })
    })
})