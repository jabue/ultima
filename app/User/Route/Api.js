const express = require('express')
const userController = require('app/User/Controller/UserController')
const Auth = require('helper/Auth')
const Acl = require('helper/Acl')

const router = express.Router()

router.get('/user', Auth, Acl, userController.getList)

router.post('/user', Acl, userController.save)

router.post('/user/token', Acl, userController.getToken)

router.get('/user/test', Acl, userController.test)

module.exports = router