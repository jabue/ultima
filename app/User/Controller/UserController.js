const config = require('config')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('app/User/Model/User')
var exports = module.exports = {}

exports.getList = function(req, res, next) {
    User.fetchAll().then(function(data) {
        res.send(data)
    })
}

exports.save = function(req, res, next) {
    new User(req.body)
        .set('password', bcrypt.hashSync(req.body.password, config.get('bcrypt.saltRounds')))
        .save()
        .then(function(user) {
            res.send(user)
        })
}

exports.getToken = function(req, res, next) {
    User.where('email', req.body.email).fetch().then(function(user) {
        if (user) {
            if (bcrypt.compareSync(req.body.password, user.get('password'))) {
                var token = jwt.sign({ email: user.get('email'), role: 'customer' }, config.get('token.privatekey'), config.get('token.options'))
                res.send(token)
            } else {
                res.send('password wrong')
            }
        } else {
            res.send('email not exist')
        }
    })
}

exports.test = function(req, res, next) {
    res.send('youyou')
}